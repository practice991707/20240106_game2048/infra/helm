# Helm-chart для развертывания тестового приложения NodeJs (Задание #240106).

* Helm chart собран на основе helm-starters pack (https://github.com/christianhuth/helm-starter)

```console
helm install my-release ./game-2048
```

## Введение


This chart bootstraps the "game-2048" on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Поддерживается

- Kubernetes 1.19+

## Установка чарта

To install the chart with the release name `my-release`:

```console
helm install my-release ./game-2048
```

These commands deploy "game-2048" on the Kubernetes cluster in the default configuration. The [Values](#values) section lists the values that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall the `my-release` deployment:

```console
helm uninstall my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.


Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```console
helm install my-release -f values.yaml ./game-2048
```

License
-------

GPLv3

Author Information
------------------

* Kirill Fedorov (tg:fedrr) as a challege for ClassBook #240106 project https://deusops.com/classbook
